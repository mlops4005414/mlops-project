# Contributing

The [mlops-project](https://gitlab.com/mlops4005414/mlops-project) is a study project within the [__ODS MLOps and production in DS research__](https://ods.ai/tracks/mlops3-course-spring-2024) course.
If you're trying to use the code or contribute to it, your experience and what you can contribute are important to the project's success.

## Code of Conduct

This project and everyone participating in it is governed by the [Code of Conduct](CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [6wtkc1zqgyjd@mail.ru](mailto:6wtkc1zqgyjd@mail.ru).

## Environment setup

Nothing easier!

Fork and clone the repository, then:

```bash
cd mlops-project
```

> NOTE:
> To continue managing the project you'll need to install
> [PDM](https://github.com/pdm-project/pdm)
> manually.
>
> You can install it with:
>
> ```bash
> python3 -m pip install --user pipx
> pipx install pdm
> ```
> or with conda:
>
> ```bash
> conda install conda-forge::pdm
> ```
>
> Now you can run `pdm install`.

You now have the dependencies installed.

### Environment details

Note the environment dependencies are locked in the `pdm.lock` file and should be installed automatically once `pdm install` is run.
For checking the commits the `pre-commit` framework is used. The configuration and the set of hooks are listed in `.pre-commit-config.yaml`

## Development

As usual:

1. create a new branch: `git switch -c feature-or-bugfix-name`
1. edit the code and/or the documentation

**Before committing:**

1. run `pre-commit` to run a set of pre-commit hooks defined in the `.pre-commit-config.yaml` (those include `ruff`, `bandit`).
1. if you've updated the documentation or the project dependencies:
    1. run `pdm update`

The linter `ruff` can be run separately:

```terminal
> ruff check
> ruff format
```

## Commit message convention

Commit messages must follow our convention based on the
[Angular style](https://gist.github.com/stephenparish/9941e89d80e2bc58a153#format-of-the-commit-message)
or the [Karma convention](https://karma-runner.github.io/4.0/dev/git-commit-msg.html):

```code
<type>[(scope)]: Subject

[Body]
```

Pls follow below [guideline](https://github.com/atom/atom/blob/master/CONTRIBUTING.md#styleguides):

* Use the present tense ("Add feature" not "Added feature")
* Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
* Limit the first line to 72 characters or less
* Reference issues and pull requests liberally after the first line
* When only changing documentation, include `[ci skip]` in the commit title
* Consider starting the commit message with an applicable emoji:
    * :art: `:art:` when improving the format/structure of the code
    * :racehorse: `:racehorse:` when improving performance
    * :non-potable_water: `:non-potable_water:` when plugging memory leaks
    * :memo: `:memo:` when writing docs
    * :penguin: `:penguin:` when fixing something on Linux
    * :apple: `:apple:` when fixing something on macOS
    * :checkered_flag: `:checkered_flag:` when fixing something on Windows
    * :bug: `:bug:` when fixing a bug
    * :fire: `:fire:` when removing code or files
    * :green_heart: `:green_heart:` when fixing the CI build
    * :white_check_mark: `:white_check_mark:` when adding tests
    * :lock: `:lock:` when dealing with security
    * :arrow_up: `:arrow_up:` when upgrading dependencies
    * :arrow_down: `:arrow_down:` when downgrading dependencies
    * :shirt: `:shirt:` when removing linter warnings

## Pull requests guidelines

Link to any related issue in the Pull Request message.

During the review, we recommend using fixups:

```bash
# SHA is the SHA of the commit you want to fix
git commit --fixup=SHA
```

Once all the changes are approved, you can squash your commits:

```bash
git rebase -i --autosquash main
```

And force-push:

```bash
git push -f
```

If this seems all too complicated, you can push or force-push each new commit,
and we will squash them ourselves if needed, before merging.

## Testing

The tests as located in `tests`

Pls use `pytest`
