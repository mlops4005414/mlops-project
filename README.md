# MLOps course project

[![pdm-managed](https://img.shields.io/badge/pdm-managed-blueviolet)](https://pdm-project.org)
[![security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)

The [mlops-project](https://gitlab.com/mlops4005414/mlops-project) is a study project within the [__ODS MLOps and production in DS research__](https://ods.ai/tracks/mlops3-course-spring-2024) course.

## Features

### Project development methodology

The project development follows [Gitlab Flow](https://about.gitlab.com/topics/version-control/what-is-gitlab-flow/) approach.

With [Gitlab Flow](https://about.gitlab.com/topics/version-control/what-is-gitlab-flow/), all features and fixes go to the **main** branch while enabling **production** and **stable** branches. GitLab Flow includes a set of [best practices](https://about.gitlab.com/topics/version-control/what-are-gitlab-flow-best-practices/) and guidelines to ensure software development teams follow a smooth process to ship features collaboratively.

## Requirements

High level requirments. For more details check out `pdm.lock`
- `Python >= 3.10`
- `pdm >= 2.12`
- `pre-commit >= 3.7`
- `ruff >= 0.3.5`
- `bandit >= 1.7.8`

## Installation

Fork and clone the repository, then:

```bash
cd mlops-project
```

## Usage

- TODO

## Contributing

Contributions are very welcome.
To learn more, see the [Contributor Guide](CONTRIBUTING.md).

## License

Distributed under the terms of the [MIT license](LICENSE),
_Mlops Project_ is free and open source software.

## Issues

If you encounter any problems,
please file an issue along with a detailed description.
